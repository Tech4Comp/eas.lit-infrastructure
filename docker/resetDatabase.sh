#!/bin/bash

username=admin
password=XYZ

# delete datasets
curl 'https://db.example.org/$/datasets/eal' -X DELETE -H "Authorization: Basic $(echo -n $username:$password | base64)"
curl 'https://db.example.org/$/datasets/knowledgeMaps' -X DELETE -H "Authorization: Basic $(echo -n $username:$password | base64)"

# create datasets
curl 'https://db.example.org/$/datasets' -X POST -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' -H "Authorization: Basic $(echo -n $username:$password | base64)" --data-raw 'dbName=eal&dbType=tdb'
curl 'https://db.example.org/$/datasets' -X POST -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' -H "Authorization: Basic $(echo -n $username:$password | base64)" --data-raw 'dbName=knowledgeMaps&dbType=tdb'

# repopulate datasets
docker restart <Item-Service-Container>
# or
#curl 'https://fuseki.easlit.aksw.org/eal?graph=http://item.easlit.aksw.org/counters' -X POST -H "Authorization: Basic $(echo -n $username:$password | base64)" -F upload=@localFile.ttl
# ...
