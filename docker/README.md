# Docker Deployment

This setup uses [docker-compose](https://docs.docker.com/compose/) and shows exemplarily how to deploy EAs.LiT version 2 using [Docker](https://docs.docker.com/engine/). To be used for any real deployment, the docker-compose.yml file must be changed according to the target environment.

The present setup expects that it is deployed without any existing infrastructure and thus sets up a [reverse-proxy server](https://hub.docker.com/r/nginxproxy/nginx-proxy) and also a service to receive [letsencrypt certificates](https://hub.docker.com/r/jrcs/letsencrypt-nginx-proxy-companion). Furthermore it expects that several subdomains (or a wildcard domain) are usable, which are used inside the docker-compose file as `[XYZ].example.com` and which need to be changed to the used domain.

By design EAs.LiT is able to send e-mails using an e-mail server, which is not provided as of this setup. If no e-mail server is provided, this capability is deactivated. Furthermore EAs.LiT is designed to use OAuth and WebID services, but will also run without these for testing purposes with deactivated user management. OAuth and WebID services are not part of this setup, as these typically already exist. Our recommendation is [Keycloak](https://www.keycloak.org/) - see the [docker image](https://registry.hub.docker.com/r/jboss/keycloak).

For more details see the docker-compose.yml file and the corresponding section comments in it.
