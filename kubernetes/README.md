# Kubernetes Deployment

This setup is directly derived from the docker sutup by using the tool [kompose](https://kompose.io/). Thus it can be used as a starting point to set up a [Kubernetes](https://kubernetes.io/) deployment. See the `~/docker/` folder for further information.
